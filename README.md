# Retail Discounter Application

The following code is used to apply appropriate discounts to a shopping bill at a retail store.

The conditions relating to the discount are as follows:
1. Employees get a 30% discount.
2. Affiliates of the store get a 10% discount.
3. If the user has been a customer for over 2 years, they gets a 5% discount.
4. For every $100 on the bill, there would be a $ 5 discount.
5. The percentage based discounts do not apply on groceries.
6. A user can get only one of the percentage based discounts on a bill.
7. The percentage based discount is applied before the $5 per $100 discount.

To test the code, simply run the file runner.sh, or the command ./gradlew build jacocoTestReport from the applicative directory.

This will compile the main and test code packages and run them, along with generating a coverage report.
The report can be found under build/reports/test/html/index.html.
