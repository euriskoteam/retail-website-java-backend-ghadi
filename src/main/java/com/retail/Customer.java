package com.retail;

import java.util.Date;


public class Customer {

    private final Date dateStarted;
    private final CustomerAffiliation affiliation;


    public Customer(CustomerAffiliation affiliation) {
        this.affiliation = affiliation;
        this.dateStarted = new Date();
    }

    public long getYearsAsCustomer() {
        return DateSubtractor.getDifferenceInYears(dateStarted);
    }

    public CustomerAffiliation getAffiliation() {
        return affiliation;
    }
}
