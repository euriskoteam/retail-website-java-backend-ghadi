package com.retail;

public enum CustomerAffiliation {

    EMPLOYEE, AFFILIATE, REGULAR

}
