package com.retail;

import java.util.Date;

public class DateSubtractor {

    private static final int MILLISECONDS_TO_SECONDS = 1000;
    private static final int SECONDS_TO_MINUTES = 60;
    private static final int MINUTES_TO_HOURS = 60;
    private static final int HOURS_TO_DAYS = 24;
    private static final int DAYS_TO_YEARS = 365;

    private DateSubtractor() {
        //Utility class
    }

    public static long getDifferenceInYears(Date dateStarted) {
        Date now = new Date();

        return (now.getTime() - dateStarted.getTime())
                / MILLISECONDS_TO_SECONDS
                / SECONDS_TO_MINUTES
                / MINUTES_TO_HOURS
                / HOURS_TO_DAYS
                / DAYS_TO_YEARS;
    }
}
