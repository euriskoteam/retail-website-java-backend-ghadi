package com.retail;

public interface Discounter {

    double discountBill(ShoppingBill shoppingBill);
}
