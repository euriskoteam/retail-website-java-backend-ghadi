package com.retail;

public class DiscounterRetail implements Discounter {

    public double discountBill(ShoppingBill shoppingBill) {
        Customer customer = shoppingBill.getCustomer();

        double discountRate = Math.max(getCustomerAffiliationDiscountRate(customer),
                getCustomerLoyaltyDiscountRate(customer));

        double totalBillBeforeFinalDiscount = shoppingBill.getNonGroceriesBill() * (1 - discountRate) + shoppingBill.getGroceriesBill();
        double finalDiscountAmount = (int) (totalBillBeforeFinalDiscount / 100) * 5;
        return totalBillBeforeFinalDiscount - finalDiscountAmount;
    }

    private double getCustomerAffiliationDiscountRate(Customer customer) {
        switch (customer.getAffiliation()) {
            case EMPLOYEE:
                return .3;
            case AFFILIATE:
                return .1;
            case REGULAR:
                return 0;
            default:
                return 0;
        }
    }

    private double getCustomerLoyaltyDiscountRate(Customer customer) {
        return customer.getYearsAsCustomer() >= 2 ? .05 : 0;
    }


}
