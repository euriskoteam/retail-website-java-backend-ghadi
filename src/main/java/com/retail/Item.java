package com.retail;

import com.google.common.base.Objects;

public class Item {

    private final String name;
    private final double cost;
    private final ItemType itemType;

    public Item(String name, double cost, ItemType itemType) {
        this.name = name;
        this.cost = cost;
        this.itemType = itemType;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }

    public ItemType getItemType() {
        return itemType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return cost == item.cost &&
                Objects.equal(name, item.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, cost);
    }
}
