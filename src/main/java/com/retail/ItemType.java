package com.retail;

public enum ItemType {

    GROCERIES, ELECTRONICS, FURNITURE
}
