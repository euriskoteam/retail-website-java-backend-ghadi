package com.retail;

import java.util.ArrayList;
import java.util.List;

import static com.retail.ItemType.GROCERIES;

public class ShoppingBill {

    private final Customer customer;
    private final List<Item> items;

    public ShoppingBill(Customer customer) {
        this.customer = customer;
        this.items = new ArrayList<>();
    }

    public Customer getCustomer() {
        return customer;
    }

    public void addItem(Item item) {
        items.add(item);
    }

    public boolean removeItem(Item item) {
        return items.remove(item);
    }

    public double getTotalBill() {
        return items.stream()
                .mapToDouble(Item::getCost).sum();
    }

    public double getGroceriesBill() {
        return items.stream()
                .filter(item -> item.getItemType() == GROCERIES)
                .mapToDouble(Item::getCost).sum();
    }

    public double getNonGroceriesBill() {
        return items.stream()
                .filter(item -> item.getItemType() != GROCERIES)
                .mapToDouble(Item::getCost).sum();
    }

}
