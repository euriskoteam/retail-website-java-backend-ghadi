package com.retail;

import org.junit.Test;
import org.mockito.Mockito;

import static com.retail.CustomerAffiliation.*;
import static com.retail.ItemType.*;
import static org.junit.Assert.assertEquals;

public class DiscounterTest {
    private static final Item CORN_FLAKES = new Item("Corn Flakes", 10, GROCERIES);
    private static final Item TELEVISION = new Item("Television", 320, ELECTRONICS);
    private static final Item COUCH = new Item("Couch", 200, FURNITURE);

    private static final Discounter DISCOUNTER = new DiscounterRetail();

    @Test
    public void percentageDiscountAlwaysAppliesBeforeTotalDiscount() {
        Customer customer = new Customer(EMPLOYEE);
        ShoppingBill shoppingBill = fillOrder(customer);

        double expectedBill = 359;

        assertEquals(expectedBill, DISCOUNTER.discountBill(shoppingBill), 3);
    }

    @Test
    public void employeeDiscountIsThirtyPercent() {
        Customer customer = new Customer(EMPLOYEE);
        ShoppingBill shoppingBill = new ShoppingBill(customer);
        shoppingBill.addItem(COUCH);

        double expectedBill = 135;

        assertEquals(expectedBill, DISCOUNTER.discountBill(shoppingBill), 3);
    }

    @Test
    public void affiliateDiscountIsTenPercent() {
        Customer customer = new Customer(AFFILIATE);
        ShoppingBill shoppingBill = fillOrder(customer);

        double expectedBill = 458;

        assertEquals(expectedBill, DISCOUNTER.discountBill(shoppingBill), 3);
    }

    @Test
    public void regularCustomersGetNoPercentageDiscount() {
        Customer customer = new Customer(REGULAR);
        ShoppingBill shoppingBill = fillOrder(customer);

        double expectedBill = 505;

        assertEquals(expectedBill, DISCOUNTER.discountBill(shoppingBill), 3);
    }

    @Test
    public void regularCustomersWithTwoYearsOfLoyaltyGetFivePercentDiscount() {
        Customer customer = Mockito.mock(Customer.class);
        Mockito.when(customer.getAffiliation()).thenReturn(REGULAR);
        Mockito.when(customer.getYearsAsCustomer()).thenReturn(2L);
        ShoppingBill shoppingBill = fillOrder(customer);

        double expectedBill = 479;

        assertEquals(expectedBill, DISCOUNTER.discountBill(shoppingBill), 3);
    }

    private ShoppingBill fillOrder(Customer employee) {
        ShoppingBill shoppingBill = new ShoppingBill(employee);
        shoppingBill.addItem(CORN_FLAKES);
        shoppingBill.addItem(TELEVISION);
        shoppingBill.addItem(COUCH);
        return shoppingBill;
    }
}
