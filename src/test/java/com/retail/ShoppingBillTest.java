package com.retail;

import org.junit.Assert;
import org.junit.Test;

import static com.retail.CustomerAffiliation.REGULAR;
import static com.retail.ItemType.FURNITURE;

public class ShoppingBillTest {

    private static final Item COUCH = new Item("Couch", 200, FURNITURE);

    @Test
    public void canRemoveItemFromShoppingList() {
        Customer customer = new Customer(REGULAR);
        ShoppingBill shoppingBill = new ShoppingBill(customer);
        shoppingBill.addItem(COUCH);
        shoppingBill.removeItem(COUCH);

        int expected = 0;

        Assert.assertEquals(expected, shoppingBill.getTotalBill(), 3);
    }

    @Test
    public void cannotRemoveItemsFromEmptyShoppingList() {
        Customer customer = new Customer(REGULAR);
        ShoppingBill shoppingBill = new ShoppingBill(customer);

        Assert.assertFalse(shoppingBill.removeItem(COUCH));
    }
}
